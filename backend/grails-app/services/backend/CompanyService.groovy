package backend

import backend.dto.CompanyDTO
import grails.gorm.transactions.Transactional

@Transactional
class CompanyService {

    List<CompanyDTO> findAll() {
        List<CompanyDTO> dtos =  Company.findAll().collect { it->
            new CompanyDTO(it.getId(), it.getName(), it.getSegment(), it.getAllTimeStandardDeviation())
        }
        dtos
    }
}
