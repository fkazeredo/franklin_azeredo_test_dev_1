package backend

import grails.gorm.transactions.Rollback
import grails.testing.mixin.integration.Integration
import grails.testing.services.ServiceUnitTest
import spock.lang.Specification

@Integration
@Rollback
class StockServiceSpec extends Specification implements ServiceUnitTest<StockService> {

    def setup() {
    }

    def cleanup() {
    }

    void "Must retrieve all Stocks from a Company"() {
        when:
        def company = Company.findByName("Apple Inc")
        def result = service.getStocks(company, 30)

        then:
        !result.isEmpty()
    }
}
