package backend

import grails.gorm.transactions.Transactional

import java.time.LocalDateTime
import java.time.LocalTime

@Transactional
class StockService {

    List<Stock> getStocks(Company company, Integer numbersOfHoursUntilNow) {

        def startTime = System.nanoTime()
        def minDate = LocalDateTime.now().minusHours(numbersOfHoursUntilNow)
        def maxDate = LocalDateTime.now()

        def criteria = Stock.createCriteria()
        List<Stock> stocks = criteria.list {
            eq("company", company)
            between("priceDate", minDate, maxDate)
            order("priceDate", "desc")
        }

        stocks.each { stock ->

            println("Quote: ${stock.getCompany().getName()} - ${stock.getPriceDate()} - ${stock.getPrice()}")

        }

        println("Number of quotes retrieved: ${stocks.size()}")

        long endTime = System.nanoTime();
        println("Total time, in milliseconds: ${(endTime - startTime)}")

        stocks

    }

}
