package backend

import grails.converters.JSON

import java.math.RoundingMode
import java.time.LocalDateTime
import java.time.LocalTime

class BootStrap {

    StockService stockService

    def init = { servletContext ->

        JSON.registerObjectMarshaller(LocalDateTime) {
            return it.toString()
        }

        List<Company> companies = this.initCompanies()
        this.initStocks(companies)

        // To print stockService.getStocks
        //Company company = Company.get(1)
        //this.stockService.getStocks(company, 12)

    }

    def destroy = {
    }

    def initCompanies = {

        Company apple = new Company(name: "Apple Inc", segment: "Technology").save()
        Company facebook = new Company(name: "Facebook, Inc", segment: "Social media").save()
        Company ford = new Company(name: "Ford", segment: "Vehicle").save()

        [apple, facebook, ford]

    }


    def initStocks = { List<Company> companies ->

        for (Company company in companies) {
            saveStocks(company)
        }

    }

    def saveStocks = { Company company ->
        LocalDateTime priceDate = LocalDateTime.now().minusMonths(1).with(LocalTime.MIDNIGHT)
        LocalDateTime now = LocalDateTime.now()

        BigDecimal initialPrice = new BigDecimal(50.0)

        while (priceDate.isBefore(now)) {

            if ((priceDate.toLocalTime().equals(LocalTime.of(10, 0, 0, 0))
                    || priceDate.toLocalTime().equals(LocalTime.of(18, 0, 0, 0)))
                    || (priceDate.toLocalTime().isAfter(LocalTime.of(10, 0, 0, 0))
                    && priceDate.toLocalTime().isBefore(LocalTime.of(18, 0, 0, 0)))) {

                Stock stock = new Stock(price: simulateStockPrice(initialPrice), priceDate: priceDate, company: company).save()

                initialPrice = stock.getPrice()

            }

            priceDate = priceDate.plusMinutes(1);

        }
    }

    def simulateStockPrice = { BigDecimal lastPrice ->
        double min = -2
        double max = 2
        Random r = new Random()
        double number = min + (max - min) * r.nextDouble()
        double finalPrice = lastPrice.toDouble() < 10 ? 10 : number
        lastPrice.add(new BigDecimal(finalPrice)).setScale(2, RoundingMode.HALF_EVEN)
    }

}
