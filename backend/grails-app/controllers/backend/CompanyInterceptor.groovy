package backend

class CompanyInterceptor {

    boolean before() {
        //My Angular app's domain is local.test, so it needs to be the allowed origin
        header( "Access-Control-Allow-Origin", "http://localhost:4200" )
        header( "Access-Control-Allow-Credentials", "true" )
        header( "Access-Control-Allow-Methods", "GET" )
        header( "Access-Control-Max-Age", "3600" )
        true
    }

    boolean after() { true }

}
