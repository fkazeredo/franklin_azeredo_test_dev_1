package backend


import java.math.RoundingMode

class Company {

    String name
    String segment

    static hasMany = [stocks: Stock]

    static mappedBy = [stocks: "company"]

    static constraints = {
        name nullable: false, blank: false, maxSize: 255
        segment nullable: false, blank: false, maxSize: 128
    }

    BigDecimal getAllTimeStandardDeviation() {
        List<Double> prices = this.stocks.collect { it -> it.getPrice().toDouble() }
        calculateAllTimeDeviation(prices)
    }

    private BigDecimal calculateAllTimeDeviation(List<Double> prices) {

        Double sum = 0.0
        Double standardDeviation = 0.0
        int length = prices.size()

        for (Double num : prices) {
            sum += num
        }

        Double mean = sum / length

        for (Double num : prices) {
            standardDeviation += Math.pow(num - mean, 2)
        }

        new BigDecimal(Math.sqrt(standardDeviation / (length - 1))).setScale(2, RoundingMode.HALF_EVEN)

    }

}
