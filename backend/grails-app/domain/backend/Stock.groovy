package backend

import java.time.LocalDateTime

class Stock {

    BigDecimal price

    LocalDateTime priceDate

    Company company

    static constraints = {
        price nullable: false
        priceDate nullable: false
        company nullable:false
    }

    static mapping = {
        company fetch: 'join'
    }

}
