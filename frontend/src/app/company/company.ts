export interface Company {
    id: number;
    name: string;
    segment: string;
    allTimeStandardDeviation: number;
}