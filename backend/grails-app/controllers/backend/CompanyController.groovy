package backend

import grails.rest.RestfulController
import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class CompanyController extends RestfulController {

    static responseFormats = ['json', 'xml']

    CompanyController() {
        super(Company)
    }

    CompanyService companyService

    List<Stock> index() {
        respond companyService.findAll()
    }


}
