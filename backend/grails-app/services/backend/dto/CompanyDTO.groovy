package backend.dto

class CompanyDTO {

    Long id
    String name
    String segment
    BigDecimal allTimeStandardDeviation

    CompanyDTO() {}

    CompanyDTO(Long id, String name, String segment, BigDecimal allTimeStandardDeviation) {
        this.id = id
        this.name = name
        this.segment = segment
        this.allTimeStandardDeviation = allTimeStandardDeviation
    }

}
