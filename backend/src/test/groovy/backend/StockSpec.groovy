package backend

import grails.testing.gorm.DomainUnitTest
import spock.lang.Specification

class StockSpec extends Specification implements DomainUnitTest<Stock> {

    def setup() {
    }

    def cleanup() {
    }

    void 'Stock price cannot be null'() {
        when:
        domain.price = null

        then:
        !domain.validate(['price'])
        domain.errors['price'].code == 'nullable'
    }


    void 'Stock price date cannot be null'() {
        when:
        domain.priceDate = null

        then:
        !domain.validate(['priceDate'])
        domain.errors['priceDate'].code == 'nullable'
    }

    void 'Stock company cannot be null'() {
        when:
        domain.company = null

        then:
        !domain.validate(['company'])
        domain.errors['company'].code == 'nullable'
    }

}
