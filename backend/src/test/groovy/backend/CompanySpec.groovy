package backend

import grails.testing.gorm.DomainUnitTest
import spock.lang.Specification

import java.math.RoundingMode
import java.time.LocalDateTime

class CompanySpec extends Specification implements DomainUnitTest<Company> {

    def setup() {
    }

    def cleanup() {
    }

    void 'Must calculate Standard Deviation for its quotes for all time.'() {
        when:
        domain.name = "Apple Inc"
        domain.segment = "Technology"
        domain.stocks = Arrays.asList(
                new Stock(price: new BigDecimal(5.0), priceDate: LocalDateTime.now()),
                new Stock(price: new BigDecimal(8.0), priceDate: LocalDateTime.now()),
                new Stock(price: new BigDecimal(10.0), priceDate: LocalDateTime.now()),
                new Stock(price: new BigDecimal(7.0), priceDate: LocalDateTime.now())
        )

        then:
        domain.getAllTimeStandardDeviation() == new BigDecimal(2.08).setScale(2, RoundingMode.HALF_EVEN)
    }

    void 'Company name cannot be null'() {
        when:
        domain.name = null

        then:
        !domain.validate(['name'])
        domain.errors['name'].code == 'nullable'
    }

    void 'Company name cannot be blank'() {
        when:
        domain.name = ''

        then:
        !domain.validate(['name'])
        domain.errors['name'].code == 'blank'
    }

    void 'Company name can have a maximum of 255 characters'() {
        when: 'for a string of 256 characters'
        String str = 'a' * 256
        domain.name = str

        then: 'name validation fails'
        !domain.validate(['name'])
        domain.errors['name'].code == 'maxSize.exceeded'

        when: 'for a string of 256 characters'
        str = 'a' * 255
        domain.name = str

        then: 'name validation passes'
        domain.validate(['name'])
    }

    void 'Company segment cannot be null'() {
        when:
        domain.segment = null

        then:
        !domain.validate(['segment'])
        domain.errors['segment'].code == 'nullable'
    }

    void 'Company segment cannot be blank'() {
        when:
        domain.segment = ''

        then:
        !domain.validate(['segment'])
        domain.errors['segment'].code == 'blank'
    }

    void 'Company segment can have a maximum of 128 characters'() {
        when: 'for a string of 129 characters'
        String str = 'a' * 129
        domain.segment = str

        then: 'segment validation fails'
        !domain.validate(['segment'])
        domain.errors['segment'].code == 'maxSize.exceeded'

        when: 'for a string of 256 characters'
        str = 'a' * 128
        domain.segment = str

        then: 'segment validation passes'
        domain.validate(['segment'])
    }

}
