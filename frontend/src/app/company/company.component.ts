import { Component, OnInit } from '@angular/core';
import { Company } from './company';
import { CompanyService } from './company.service';

@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.scss']
})
export class CompanyComponent implements OnInit {

  companies: Array<Company> = [];
  buttonClicked = false;
  serverError = false;

  constructor(private companyService: CompanyService) { }

  ngOnInit() {
  }

  listCompanies() {
    this.buttonClicked = true;
    this.companyService.getCompanies()
      .then((data: Array<Company>) => {
        this.companies = data;
      }).catch(() => this.serverError = true);
  }

}

